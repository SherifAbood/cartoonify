import numpy as np
import cv2
import dlib
import imutils

class FaceDetector:

    dlib_detector = dlib.get_frontal_face_detector()
    dlib_predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

    def rectangle_to_bounding_box(self, rect):
    	# take a bounding predicted by dlib and convert it
        # to the format (x, y, w, h) as we would normally do
        # with OpenCV
        x = rect.left()
        y = rect.top()
        w = rect.right() - x
        h = rect.bottom() - y
    
        # return a tuple of (x, y, w, h)
        return (x, y, w, h)
    
    
    def shape_to_np_array(self, shape, dtype="int"):
        # initialize the list of (x, y)-coordinates
        coords = np.zeros((68, 2), dtype=dtype)
    
        # loop over the 68 facial landmarks and convert them
        # to a 2-tuple of (x, y)-coordinates
        for i in range(0, 68):
            coords[i] = (shape.part(i).x, shape.part(i).y)
    
        # return the list of (x, y)-coordinates
        return coords

    def detect_face_and_landmarks(self, image_path, filename):
        # load the input image, resize it, and convert it to grayscale
        image = cv2.imread(image_path)
        image = imutils.resize(image, width=500)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        
        # detect faces in the grayscale image
        rects = self.dlib_detector(gray, 1)
        # loop over the face detections
        for (i, rect) in enumerate(rects):
            # determine the facial landmarks for the face region, then
            # convert the facial landmark (x, y)-coordinates to a NumPy
            # array
            shape = self.dlib_predictor(gray, rect)
            shape = self.shape_to_np_array(shape)
        
            # convert dlib's rectangle to a OpenCV-style bounding box
            # [i.e., (x, y, w, h)], then draw the face bounding box
            (x, y, w, h) = self.rectangle_to_bounding_box(rect)
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        
            # show the face number
            cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        
            # loop over the (x, y)-coordinates for the facial landmarks
            # and draw them on the image
            for (x, y) in shape:
                cv2.circle(image, (x, y), 1, (0, 0, 255), -1)
        
        # show the output image with the face detections + facial landmarks
        cv2.imwrite(filename, image)
        