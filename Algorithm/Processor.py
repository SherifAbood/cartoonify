import cv2 as cv
import numpy as np
import math
import dlib
import imutils

class Processor:
	img=''
	path=''
	def __init__(self, path):
		self.path=path

	def LoadImg(path):
		self.img = cv.imread(path,1)




def OverlayImage(src, overlay, dy, dx,face):

	for y in range(overlay.shape[0]):

		if y+dy < src.shape[0] and((y+dy<(face[41][1]+face[40][1])/2 and y+dy>(face[37][1]+face[38][1])/2) or (y+dy<(face[46][1]+face[47][1])/2 and y+dy>(face[43][1]+face[44][1])/2)):

			for x in range(overlay.shape[1]):

				if x+dx < src.shape[1] and((x+dx>(face[41][0]+face[36][0])/2 and x+dx<(face[39][0]+face[40][0])/2) or (x+dx>(face[47][0]+face[42][0])/2 and x+dx<(face[46][0]+face[45][0])/2)) :
					print (overlay[x,y,:])
					if overlay[x,y,0]<240 and overlay[x,y,1]<240 and overlay[x,y,2]<240:
						src[y+dy ,x+dx,0] = overlay[x,y,0]
						src[y+dy ,x+dx,1] = overlay[x,y,1]
						src[y+dy ,x+dx,2] = overlay[x,y,2]